package containers

import actions.InvalidateSubreddit
import actions.SelectSubreddit
import actions.fetchPostsIfNeeded
import components.picker
import components.posts
import kotlinext.js.js
import kotlinx.html.js.onClickFunction
import kotlinx.html.style
import org.w3c.dom.events.Event
import react.*
import react.dom.*
import react.redux.DispatchProps
import react.redux.rConnect
import reducers.State
import redux.RAction
import redux.WrapperAction
import kotlin.js.Date

class AsyncApp(props: Props) : RComponent<AsyncApp.Props, RState>(props) {
    data class Props(
            var selectedSubreddit: String,
            var posts: Array<String>,
            var isFetching: Boolean,
            var lastUpdated: Date,
            var dispatch: (dynamic) -> Unit
    ) : RProps

    override fun componentDidMount() {
        props.dispatch(fetchPostsIfNeeded(props.selectedSubreddit))
    }

    override fun componentDidUpdate(prevProps: Props, prevState: RState, snapshot: Any) {
        if (this.props.selectedSubreddit != prevProps.selectedSubreddit) {
            props.dispatch(fetchPostsIfNeeded(props.selectedSubreddit))
        }
    }

    private fun handleChange(nextSubreddit: String) {
        props.dispatch(SelectSubreddit(nextSubreddit))
        props.dispatch(fetchPostsIfNeeded(nextSubreddit))
    }

    private fun handleRefreshClick(e: Event) {
        e.preventDefault()

        props.dispatch(InvalidateSubreddit(props.selectedSubreddit))
        props.dispatch(fetchPostsIfNeeded(props.selectedSubreddit))
    }

    override fun RBuilder.render() {
        div {
            // TODO: onchange
            picker(value = props.selectedSubreddit,
                    onChange = ::handleChange,
                    options = arrayOf("reactjs", "frontend")
            )
        }

        p {
            span {
                +"Last updated at ${props.lastUpdated}"
            }

            if (!props.isFetching) {

                button {
                    attrs {
                        onClickFunction = ::handleRefreshClick
                    }
                    +"Refresh"
                }

            }
        }

        if (props.isFetching && props.posts.isEmpty()) { h2 { +"Loading..." } }
        else if (!props.isFetching && props.posts.isEmpty()) { h2 { +"Empty." } }
        else {
            div {
                attrs {
                    style = js {
                        opacity = if (props.isFetching) 0.5 else 1
                    }
                }

                posts(posts = props.posts)
            }
        }

    }
}

fun RBuilder.asyncApp(selectedSubreddit: String = "reactjs",
                      posts: Array<String> = arrayOf(),
                      isFetching: Boolean = false,
                      lastUpdated: Date = Date()) = child(AsyncApp::class) {
    attrs.selectedSubreddit = selectedSubreddit
    attrs.posts = posts
    attrs.isFetching = isFetching
    attrs.lastUpdated = lastUpdated
}

val connectedComponent: RClass<AsyncApp.Props> =
        rConnect<State, RAction, WrapperAction, AsyncApp.Props, AsyncApp.Props, DispatchProps<*, *>, AsyncApp.Props>(
                mapStateToProps = { state, ownProps ->
                    selectedSubreddit = state.selectedSubreddit
                    val postsBySubreddit = state.postsBySubreddit[state.selectedSubreddit]
                    if (postsBySubreddit != null) {
                        isFetching = postsBySubreddit.isFetching
                        lastUpdated = postsBySubreddit.lastUpdated
                        posts = postsBySubreddit.items
                    }
                    else {
                        isFetching = true
                        posts = arrayOf()
                    }
                },
                mapDispatchToProps = { dispatch, ownProps ->
                    this.dispatch = dispatch.asDynamic()
                }
        )(AsyncApp::class.js as RClass<AsyncApp.Props>)