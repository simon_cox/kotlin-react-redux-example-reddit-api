package containers

import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.redux.provider
import reducers.State

val theStore = store.configureStore(State())

class Root : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        provider(store = theStore) {
//            asyncApp()
            connectedComponent {}
        }
    }
}

fun RBuilder.root() = child(Root::class) {}
