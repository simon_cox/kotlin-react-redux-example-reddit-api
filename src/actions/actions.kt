package actions

import reducers.State
import redux.RAction
import kotlin.js.Date
import kotlin.browser.*


data class SelectSubreddit(val subreddit: String) : RAction
data class InvalidateSubreddit(val subreddit: String) : RAction
data class RequestPosts(val subreddit: String) : RAction
data class ReceivePosts(val subreddit: String, val json: dynamic) : RAction {
    val posts: Array<String> = json.data.children.map { child -> child.data.title } as Array<String>
    val receivedAt = Date(Date.now())
}

fun fetchPosts(subreddit: String): ((dynamic) -> Unit) -> Unit {
    return { dispatch: (dynamic) -> Unit ->
        dispatch(RequestPosts(subreddit))
        window.fetch("https://www.reddit.com/r/$subreddit.json")
                .then { response -> response.json() }
                .then { json -> dispatch(ReceivePosts(subreddit, json)) }
    }
}

fun shouldFetchPosts(state: State, subreddit: String): Boolean {
    val posts = state.postsBySubreddit[subreddit]
    return when {
        posts == null -> true
        posts.isFetching -> false
        else -> posts.didInvalidate
    }
}

fun fetchPostsIfNeeded(subreddit: String): ((dynamic) -> Unit, () -> State) -> Unit {
    return { dispatch: (dynamic) -> Unit, getState: () -> State ->
        if (shouldFetchPosts(getState(), subreddit)) {
            dispatch(fetchPosts(subreddit))
        }
    }
}