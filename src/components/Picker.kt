package components

import kotlinx.html.js.onChangeFunction
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.h1
import react.dom.option
import react.dom.select
import react.dom.span

class Picker : RComponent<Picker.Props, RState>() {
    data class Props(
            var value: String,
            var onChange: (String) -> Unit,
            var options: Array<String>
    ) : RProps

    override fun RBuilder.render() {
        span {
            h1 { props.value }
            select {
                attrs {
                    onChangeFunction = { e -> props.onChange(e.target.asDynamic().value as String) }
                    value = props.value
                }
                props.options.forEach {
                    option {
                        attrs.value = it
                        attrs.value = it
                        +it
                    }
                }
            }
        }
    }
}


fun RBuilder.picker(value: String, onChange: (String) -> Unit, options: Array<String>) = child(Picker::class) {
    attrs.value = value
    attrs.onChange = onChange
    attrs.options = options
}
