package components

import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.key
import react.dom.li
import react.dom.ul

class Posts : RComponent<Posts.Props, RState>() {
    override fun RBuilder.render() {
        ul {
            props.posts.forEachIndexed { i, post ->
                li {
                    attrs.key = i.toString()
                    // TODO: title field in Post object
                    +post
                }
            }
        }
    }

    data class Props(var posts: Array<String>): RProps
}

fun RBuilder.posts(posts: Array<String>) = child(Posts::class) {
    attrs.posts = posts
}
