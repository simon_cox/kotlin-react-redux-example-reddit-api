package store

import ktypings.redux.ReduxThunk
import reducers.State
import reducers.rootReducerFunc
import redux.*


//val loggerMiddleware = createLogger()

fun configureStore(preloadedState: State): Store<State, RAction, WrapperAction> {
    return createStore(
            ::rootReducerFunc,
            preloadedState,
            compose(applyMiddleware(ReduxThunk/*, loggerMiddleware*/), rEnhancer())
    )
}