package reducers

import redux.combineReducers
import actions.*
import redux.RAction
import redux.Reducer
import kotlin.js.Date

data class State(
        val selectedSubreddit: String = "reactjs",
        val postsBySubreddit: Map<String, PostsState> = mapOf()
)

fun selectedSubreddit(state: String = "reactjs", action: RAction): String {
    return when (action) {
        is SelectSubreddit -> action.subreddit
        else -> state
    }
}

data class PostsState(val isFetching: Boolean = false,
                      val didInvalidate: Boolean = false,
                      val items: Array<String> = arrayOf(),
                      val lastUpdated: Date = Date(Date.now()))


fun posts(state: PostsState = PostsState(), action: RAction): PostsState {
    return when (action) {
        is InvalidateSubreddit -> state.copy(didInvalidate = true)
        is RequestPosts -> state.copy(isFetching = true, didInvalidate = false)
        is ReceivePosts -> state.copy(
                isFetching = false,
                didInvalidate = false,
                items = action.posts,
                lastUpdated = action.receivedAt
        )
        else -> state
    }
}

fun postsBySubreddit(state: Map<String, PostsState> = mapOf(), action: RAction): Map<String, PostsState> {
    return when (action) {
        is InvalidateSubreddit -> state + (action.subreddit to posts(state[action.subreddit] ?: PostsState(), action))
        is ReceivePosts        -> state + (action.subreddit to posts(state[action.subreddit] ?: PostsState(), action))
        is RequestPosts        -> state + (action.subreddit to posts(state[action.subreddit] ?: PostsState(), action))
        else -> state
    }
}

val rootReducer: Reducer<State, RAction> = combineReducers(reducers = mapOf(
        "selectedSubreddit" to ::selectedSubreddit,
        "postsBySubreddit" to ::postsBySubreddit
))

fun rootReducerFunc(state: State, action: RAction): State {
    // TODO: figure out how to use combineRedcuers
    return State(
            selectedSubreddit = selectedSubreddit(state.selectedSubreddit, action),
            postsBySubreddit = postsBySubreddit(state.postsBySubreddit, action)
    )
}